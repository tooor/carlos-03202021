Emoji Search
---

emoji-search is a small application that helps you find the best emojis!

### Local Development
Install dependencies
```
npm install
```

Start development server 
```
npm start
```

## Building and Running with Docker
We can use Docker to generate a container image which encompasses our web server and static HTML files.

First build the container image using the docker build command:
```
docker build -f deployments/Dockerfile -t carlos/emoji-search .
```

Then run the container while binding the applications listening port (5000) to localhost:
```
docker run --rm --name emoji-search -it -p 3000:3000 carlos/emoji-search
```

## Run in Kubernetes - Kubectl (yaml)

Note: You must already have a Kubernetes cluster configured and ready for use! You can verify this using `kubectl get nodes`

The k8s.yaml file includes the resources necessary to deploy the emoji-search application to Kubernetes.

First install the resources to your Kubernetes cluster using kubectl:
```
kubectl apply -f k8s.yaml
```

Then start a port forward
```
kubectl port-forward svc/emoji-search 3000:3000
```

Then navigate to http://localhost:3000/ in your browser to access the application.

To clean up the created resources:
```
kubectl delete -f k8s.yaml
```


## Build and Run in Kubernetes - Tilt

Tilt can be used to run the emoji-search application in a local Kubernetes development environment.

The following command will start the application with live reload features enabled.
```
tilt up
```

## Security Improvements Implemented
- Pinned upstream container images to a specific version and checksum in Dockerfile.
- Provided a minimal webserver with no external dependencies.
- Disabled directory file listings for the static site server.
- Used multi-stage to build a minimal scratch container image with no unnecessary files.
- Set up AWS credentials as environment variables instead of hard coding them.

## Security Improvement Suggestions
- Implement 'npm audit' in CI setup to scan the project for security vulnerabilities.
- Switch to a local npm registry mirror for all dependencies (With strict read / write audit trail)
- Use npm dedupe to simplify dependency tree.
- Install SSL/TLS certificate for clients to validate site authenticity and enable encryption.
- Implement security HTTP Headers such as
    - X-Frame-Options: SAMEORIGIN
    - X-XSS-Protection: 1; mode=block
    - X-Content-Type-Options: nosniff
    - Content-Type: text/html; charset=utf-8
- Add stage to CI setup to check for possible leaked keys / secrets.
- Add stage to ensure all UI errors are disabled for clients. - Remove any console.log() 
- Scan docker container image for known CVE's. Implement OWASP Dependency-Check in CI pipeline.
- Add strict audit trail and logging for the K8s cluster. Logs must be shipped in realtime to another system.
- Linux server hardening on K8s nodes - Implement OS best practices such as CIS Benchmarks.
- Use IDS on K8s cluster (ie. Ossec, Tripwire) - Monitor file, user, permission changes & tampering 
- Move AWS credentials to a KMS such as Hashicorp Vault. Credentials should be kept separate from the CI/CD server and should be easy to rotate.
- Review OWASP Secure Coding Practices and apply to the project.

## Further Improvements 

- Implement a container vulnerability scanner tool such as Clair or Trivy in the CI pipeline. 
- Implement automatic semantic versioning
