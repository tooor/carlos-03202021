package main

// Code based on: https://www.alexedwards.net/blog/serving-static-sites-with-go

import (
	"flag"
	"log"
	"net/http"
)

var port string
var staticFilesDir string

func init() {
	flag.StringVar(&port, "p", "3000", "Web server listening port")
	flag.StringVar(&staticFilesDir, "f", "/app/html", "Static HTML files path")
}

func main() {
	flag.Parse()

	// Serve static files from directory
	fs := http.FileServer(http.Dir(staticFilesDir))

	// Handle requests to / with logging middleware
	http.Handle("/", logRequest(fs))

	log.Printf("Web server starting on port %v \n", port)
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		log.Fatal("Error Starting the HTTP Server : ", err)
		return
	}
}

// Logging Middleware
func logRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s %s\n", r.RemoteAddr, r.Method, r.URL)
		next.ServeHTTP(w, r)
	})
}
